library(here)
library(data.table)
library(dplyr)
library(lubridate)

# load data ####
load(here("data", "clean", "antibiotics.RData"))
lup_antibiotics <- readxl::read_excel(here("data", "antibiotics.xlsx"))
lup_aware <- readxl::read_excel(here("data", "WHO-aware.xlsx"), 
                                sheet = "AWaRe classification 2021", 
                                skip = 2)
# data transform ####

## prepare AWARE categories ####
setDT(lup_aware)
lup_aware <- lup_aware[, .(abx_atc = `ATC code`, aware_cat = Category)]
lup_aware$aware_cat <- tolower(lup_aware$aware_cat)

## dates ####

# convert dates to day only (without time information) to generate per day data of antibiotic consumption
# first rename existing date variable to convention of naming datetime variables with ending _dt
data.table::setnames(abx, "abx_date", "abx_date_dt")

# convert date.time variable to date only
abx <- abx %>% 
  mutate(abx_date_d = lubridate::ymd(
    paste0(
      lubridate::year(abx_date_dt), "-",
      lubridate::month(abx_date_dt), "-",
      lubridate::day(abx_date_dt))
  ))

# generate new variables for year and month of antibiotic administration
abx <- abx %>% 
  mutate(abx_date_y = lubridate::year(abx_date_d),
         abx_date_m = lubridate::month(abx_date_d, label = TRUE, abbr = FALSE))

# transform to cdc (days of administration per month and year per substance (atc code)) ####
# First step, calculate how many doses of a specific antibiotic (atc-code) and per day an individual patient received on a specific date
abx_dosesperday_dplyr <- abx %>% 
  group_by(pseudoid_fid, abx_atc, abx_date_d, abx_date_y, abx_date_m,
           abx_givenat_2) %>% 
  count() %>%
  ungroup()

# abx_dosesperday_dt <- abx[, .N, by = .(pseudoid_fid, abx_atc, abx_date_d, abx_date_y, abx_date_m)]

cdc_m <- abx_dosesperday_dplyr %>% 
  group_by(abx_atc, abx_date_y, abx_date_m, abx_givenat_2) %>% 
  count() %>%
  ungroup()

cdc_y <- abx_dosesperday_dplyr %>% 
  group_by(abx_atc, abx_date_y, abx_givenat_2) %>% 
  count() %>%
  ungroup()

# add generic names
cdc_m <- cdc_m %>% 
  left_join(lup_antibiotics, by = "abx_atc")
cdc_y <- cdc_y %>% 
  left_join(lup_antibiotics, by = "abx_atc")

# add WHO aware categories
cdc_m <- cdc_m %>%
  left_join(lup_aware, by = "abx_atc")
cdc_y <- cdc_y %>%
  left_join(lup_aware, by = "abx_atc")
  
# save data ####
save(cdc, file = here("data", "clean", "antibiotics_cdc.RData"))
