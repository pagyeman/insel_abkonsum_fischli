---
title: "verify_neonatal_data"
author: "Klara"
date: "12/6/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, error = FALSE)
```

```{r load_data}
library(here)
library(readxl)
library(tidyverse)
library(data.table)

neonates <- read_excel(here("data", "20211102_neodaten_korrektur.xlsx"),
                       sheet = "neodata_med", na = "NULL")

load(here("data", "clean", "neo_antibiotics_cdc.RData"))

setDT(neonates)
```

# ueberpruefen der plausibilitaet der daten der neonatologie
Wir haben insgesamt `r neonates[, .N]` Eintraege in der Datenbank. Davon haben `r neonates[!is.na(amoxicillin) & (!is.na(amox_start) | !is.na(amox_end)), .N]` Kinder Amoxicillin, `r neonates[!is.na(amikacin) & (!is.na(amik_start) | !is.na(amik_end)), .N]` Amikacin, 

Nach Kontrolle der Daten durch mich, haben wir aber nur `r length(unique(neo_abx[substance == "Amoxicillin", pseudo_fid]))` bei denen tatsaechlich Amoxicillin verabreicht wurde 
